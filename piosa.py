from flask import Flask, render_template, redirect, url_for, request
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView
#Seguridad:
from flask_security import UserMixin, RoleMixin, SQLAlchemyUserDatastore, \
                    Security, login_required, current_user, roles_required
from flask_security.utils import hash_password

import json
#Para recorrer diferentes listas en el mismo for
import itertools
# Para recorrer listas de manera aleatoria
import random
#para el path de las imagenes de galeria
import os
import os.path as op
# Se importa la biblioteca de flask-fileadmin modificada
import flask_fileadmin as fileadmin
# Para mover imagenes
import shutil
from sqlalchemy import event

# Para hacer migraciones
from flask_migrate import Migrate

# Production server
from waitress import serve

app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('config.py')

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Configuración de Flask-Security
app.config['USER_ENABLE_EMAIL'] = False
app.config['SECURITY_PASSWORD_HASH'] = 'bcrypt'
app.config['SECURITY_REGISTERABLE'] = False

db = SQLAlchemy(app)

migrate = Migrate(app, db)

roles_users = db.Table('roles_users',
db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
db.Column('role_id', db.Integer, db.ForeignKey('role.id'))
)

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False, server_default='')
    active =  db.Column(db.Boolean, default=True) #, nullable=False, server_default='0')
    roles = db.relationship('Role', secondary=roles_users, 
        backref=db.backref('users'))

    def __str__(self):
        return self.email

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True)
    descripcion = db.Column(db.String(255), unique=True)

    def __str__(self):
        return self.name

user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

# Tabla para la relación emprendimientos-atractivos

emprendimientos_atractivos = db.Table('emprendimientos_atractivos',
    db.Column('emp_id', db.Integer, db.ForeignKey('emprendimientos.id')),
    db.Column('atract_id', db.Integer, db.ForeignKey('atractivos.id'))
    )

# Tabla para la relación emprendimientos-servicios

emprendimientos_servicios = db.Table('emprendimientos_servicios',
    db.Column('emp_id', db.Integer, db.ForeignKey('emprendimientos.id')),
    db.Column('serv_id', db.Integer, db.ForeignKey('servicios.id'))
    )

# Tabla para la relación actractivos-regiones

atractivos_regiones = db.Table('atractivos_regiones',
    db.Column('atract_id', db.Integer, db.ForeignKey('atractivos.id')),
    db.Column('reg_id', db.Integer, db.ForeignKey('regiones.id'))
    )

# Tabla para la relación actractivos-tipos

atractivos_tipos = db.Table('atractivos_tipos',
    db.Column('atract_id', db.Integer, db.ForeignKey('atractivos.id')),
    db.Column('tipo_id', db.Integer, db.ForeignKey('tipos.id'))
    )

# Tabla para la relación eventos-tipos

eventos_tipos = db.Table('eventos_tipos',
    db.Column('event_id', db.Integer, db.ForeignKey('atractivos.id')),
    db.Column('tipo_id', db.Integer, db.ForeignKey('tiposeventos.id'))
    )

# ***** Tablas de la base de datos

class Textos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Inicio = db.Column(db.String())
    TituloEncabezado = db.Column(db.String())
    SubTituloEncabezado = db.Column(db.String())
    TextoSubEncabezado = db.Column(db.String())
    Atractivos = db.Column(db.String())
    Regiones = db.Column(db.String())
    Emprendimientos = db.Column(db.String())
    eventos = db.Column(db.String())
    mapa = db.Column(db.String())
    AcercaDe = db.Column(db.String())
    textoAcercaDe = db.Column(db.String())
    textoCreditos = db.Column(db.String())
    emprendedor = db.Column(db.String())

class Emprendimientos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(), nullable=False)
    cedulaJuridica = db.Column(db.String())
    persona = db.Column(db.String())
    descripcion = db.Column(db.String())
    costo = db.Column(db.Integer)
    imagen = db.Column(db.String())
    atractivos = db.relationship('Atractivos', secondary=emprendimientos_atractivos, 
                            backref=db.backref('emprendimientos', lazy=True))
    servicios = db.relationship('Servicios', secondary=emprendimientos_servicios, 
                            backref=db.backref('emprendimientos', lazy=True))
    region_id = db.Column(db.Integer, db.ForeignKey('regiones.id'))
    ubicacion = db.Column(db.String())
    sitioWeb = db.Column(db.String())

    def __str__(self):
        return self.nombre

class Atractivos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(), nullable=False)
    descripcion = db.Column(db.String())
    imagen = db.Column(db.String())
    ubicacion = db.Column(db.String())
    tipos = db.relationship('Tipos', secondary=atractivos_tipos, 
                            backref=db.backref('atractivos', lazy=True))
    regiones = db.relationship('Regiones', secondary=atractivos_regiones, 
                            backref=db.backref('atractivos', lazy=True))
    sitioWeb = db.Column(db.String())
    
    def __str__(self):
        return self.nombre

class Evento(Atractivos):
    id = db.Column(db.Integer, db.ForeignKey('atractivos.id'), primary_key=True)

    inicio = db.Column(db.Date, nullable=False)
    final = db.Column(db.Date, nullable=False)
    tipo_evento = db.relationship('Tiposeventos', secondary=eventos_tipos, 
                            backref=db.backref('eventos', lazy=True))

    def __str__(self):
        return self.nombre    

class Tipos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(), nullable=False)
    descripcion = db.Column(db.String())

    def __str__(self):
        return self.nombre

class Tiposeventos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(), nullable=False)
    descripcion = db.Column(db.String())

    def __str__(self):
        return self.nombre
    
        
class Servicios(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(), nullable=False)
    descripcion = db.Column(db.String())
    ubicacion = db.Column(db.String())

    def __str__(self):
        return self.nombre

class Regiones(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(), nullable=False)
    descripcion = db.Column(db.String())
    ubicacion = db.Column(db.String())
    imagen = db.Column(db.String())
    emprendimientos = db.relationship('Emprendimientos', backref='region')
    
    def __str__(self):
        return self.nombre

class Info(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String())
    descripcion = db.Column(db.Text(500))
    imagen = db.Column(db.String())

class Footer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    titulo = db.Column(db.String())
    email = db.Column(db.String())
    tel = db.Column(db.String())
    facebook = db.Column(db.String())
    youtube = db.Column(db.String())
    instagram = db.Column(db.String())
    sitio_web = db.Column(db.String())

#************* Modelos bonitos para /admin

class MyAdminIndexView(AdminIndexView):
    def is_accessible(self):
        return current_user.has_role('creador') or current_user.has_role('admin')

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('security.login'))

class MyModelView(ModelView):
    def is_accessible(self):
        return current_user.has_role('admin') or current_user.has_role('creador')

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('security.login'))

class ModelUser(ModelView):
    def is_accessible(self):
        return current_user.has_role('creador')

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('security.login'))

    column_display_pk = True
    column_hide_backrefs = False
    column_list = ('email', 'roles', 'password')

class ModelRole(ModelView):
    def is_accessible(self):
        return current_user.has_role('creador') 

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('security.login'))

class ModelEmprendimientos(MyModelView):
    column_display_pk = True
    column_hide_backrefs = False
    column_list = ('id', 'nombre', 'cedulaJuridica', 'persona', 'descripcion', 
        'costo', 'atractivos', 'servicios', 'region', 'ubicacion', 'sitioWeb')

class ModelAtractivos(MyModelView):
    column_display_pk = True
    column_hide_backrefs = False
    column_list = ('id', 'nombre', 'descripcion', 'tipo', 'imagen', 
                        'emprendimientos', 'regiones', 'ubicacion', 'sitioWeb')

class ModelTipos(MyModelView):
    column_display_pk = True
    column_hide_backrefs = False
    column_list = ('id', 'nombre', 'descripcion', 'atractivos')

class ModelTiposEventos(MyModelView):
    column_display_pk = True
    column_hide_backrefs = False
    column_list = ('id', 'nombre', 'descripcion', 'eventos')

class ModelServicios(MyModelView):
    column_display_pk = True
    column_hide_backrefs = False
    column_list = ('id', 'nombre', 'descripcion', 'imagen', 'emprendimientos', 
                   'ubicacion')

class ModelRegiones(MyModelView):
    column_display_pk = True
    column_hide_backrefs = False
    column_list = ('id', 'nombre', 'descripcion', 'ubicacion', 'imagen', 
                    'emprendimientos', 'atractivos')


# Se crea la página de admin y se agregan las vistas
admin = Admin(app, index_view=MyAdminIndexView())   

admin.add_view(ModelUser(User, db.session))
admin.add_view(ModelRole(Role, db.session))
admin.add_view(MyModelView(Textos, db.session, 'Textos'))
admin.add_view(ModelEmprendimientos(Emprendimientos, db.session))
admin.add_view(ModelAtractivos(Atractivos, db.session))
admin.add_view(ModelTipos(Tipos, db.session, 'Tipos de atractivos'))
admin.add_view(MyModelView(Evento, db.session, 'Eventos'))
admin.add_view(ModelTiposEventos(Tiposeventos, db.session, 'Tipos de eventos'))
admin.add_view(ModelServicios(Servicios, db.session))
admin.add_view(ModelRegiones(Regiones, db.session))
admin.add_view(MyModelView(Footer, db.session, 'Pie de página'))

def getPath():
    ''' Devuelve la ubicacion del script.'''
    return os.path.dirname(os.path.realpath(__file__))

# Se crea la vista en la que se subirán imagenes
# dirección
path = op.join(getPath(), 'static/img')
try:
    os.mkdir(path)
except OSError:
    pass

admin.add_view(fileadmin.FileAdmin(path, '/files/', name='Imágenes'))

db.create_all() 

# Asignando las imágenes principales de los emprendimientos, atractivos y regiones a partir
# de las imágenes en el directorio:
def link_a_imagen():
    for emprendimiento in Emprendimientos.query.all():
        try:
            path = os.listdir(getPath() +'/static/img/emprendimientos/' 
                        + str(emprendimiento.nombre))
            imagenes = [getPath() + '/static/img/emprendimientos/' 
                        + str(emprendimiento.nombre) + "/" + file for file in path]
            for imagen in imagenes:
                if len(imagen.split(".")) >= 2:
                    emprendimiento.imagen = '/static/img/emprendimientos/' 
                    + imagen.split('/static/img/emprendimientos/')[1]
            db.session.commit()
        except:
            pass

    for atractivo in Atractivos.query.all():
        try:
            path = os.listdir(getPath() +'/static/img/atractivos/' 
                        + str(atractivo.nombre))
            imagenes = [getPath() + '/static/img/atractivos/' 
                        + str(atractivo.nombre) + "/" + file for file in path]
            for imagen in imagenes:
                if len(imagen.split(".")) >= 2:
                    atractivo.imagen = '/static/img/atractivos/' 
                    + imagen.split('/static/img/atractivos/')[1]
            db.session.commit()
        except:
            pass

    for region in Regiones.query.all():
        try:
            path = os.listdir(getPath() +'/static/img/regiones/' 
                        + str(region.nombre))
            imagenes = [getPath() + '/static/img/regiones/' 
                        + str(region.nombre) + "/" + file for file in path]
            for imagen in imagenes:
                if len(imagen.split(".")) >= 2:
                    region.imagen = '/static/img/regiones/' 
                    + imagen.split('/static/img/regiones/')[1]
            db.session.commit()
        except:
            pass


def crearCarpeta(tipo, nombre):
    try:
        os.makedirs(getPath() + '/static/img/' + tipo + "/" + nombre + '/otras/')    
    except FileExistsError:
        pass


def eliminarCarpeta(tipo, nombre):
    try:
        shutil.rmtree(getPath() + '/static/img/' + tipo + "/" + nombre + "/") 
    except:
        pass


def moverImagenes(tipo, fuente, destino):

    crearCarpeta(tipo, destino)

    # Se mueve la imagen principal
    dir_fuente = getPath() + '/static/img/' + tipo + "/" + fuente
    dir_destino = getPath() + '/static/img/' + tipo + "/" + destino
    imagenes = os.listdir(dir_fuente)   
    for file_name in imagenes:
        if "." in file_name:
            shutil.copy(os.path.join(dir_fuente, file_name), dir_destino)

    # Se mueven las imagenes en /otras
    dir_fuente = getPath() + '/static/img/' + tipo + "/" + fuente + "/otras/"
    dir_destino = getPath() + '/static/img/' + tipo + "/" + destino + "/otras/"
    imagenes = os.listdir(dir_fuente)    
    for file_name in imagenes:
        if "." in file_name:
            shutil.move(os.path.join(dir_fuente, file_name), dir_destino)

    # Se elimina el directorio antiguo
    eliminarCarpeta(tipo, fuente)


# Listeners para Emprendimientos
@event.listens_for(Emprendimientos.nombre, 'set')
def receive_set(target, value, oldvalue, initiator):
    if isinstance(oldvalue, str) and oldvalue != value:
        moverImagenes("emprendimientos", oldvalue, value)
    else:
        crearCarpeta("emprendimientos", value)

@event.listens_for(Emprendimientos, 'after_delete')
def receive_after_delete(mapper, connection, target):
    eliminarCarpeta("emprendimientos", target.nombre)


# Listeners para Atractivos
@event.listens_for(Atractivos.nombre, 'set')
def receive_set(target, value, oldvalue, initiator):
    if isinstance(oldvalue, str) and oldvalue != value:
        moverImagenes("atractivos", oldvalue, value)
    else:
        crearCarpeta("atractivos", value)

@event.listens_for(Atractivos, 'after_delete')
def receive_after_delete(mapper, connection, target):
    eliminarCarpeta("atractivos", target.nombre)


# Listeners para Regiones
@event.listens_for(Regiones.nombre, 'set')
def receive_set(target, value, oldvalue, initiator):
    if isinstance(oldvalue, str) and oldvalue != value:
        moverImagenes("regiones", oldvalue, value)
    else:
        crearCarpeta("regiones", value)

@event.listens_for(Regiones, 'after_delete')
def receive_after_delete(mapper, connection, target):
    eliminarCarpeta("regiones", target.nombre)


# Listeners para Eventos
@event.listens_for(Evento.nombre, 'set')
def receive_set(target, value, oldvalue, initiator):
    if isinstance(oldvalue, str) and oldvalue != value:
        moverImagenes("atractivos", oldvalue, value)
    else:
        crearCarpeta("atractivos", value)

@event.listens_for(Evento, 'after_delete')
def receive_after_delete(mapper, connection, target):
    eliminarCarpeta("atractivos", target.nombre)

# Rutas

# Para crear un nuevo usuario
@app.route('/crear/', methods=['POST', 'GET'])
@roles_required("creador")
def crear():
    if request.method == 'POST':
        try:
            # Se confirma que las contraseñas sean iguales
            password=request.form.get('pass')
            repeat_password=request.form.get('pass2')
            if (password == repeat_password):    
                user_datastore.create_user(email=request.form.get('email'),
                password=hash_password(request.form.get('pass')))
                db.session.commit()
            else:
                return "Las contraseñas son distintas"
            return redirect('/admin/user/')
        except:
            return("Algo salió mal")
    return render_template("security/crear_usuario.html", texts=Textos.query.all(), 
                            footer=Footer.query.all())

@app.route("/")
@app.route("/home/")
def home():
    link_a_imagen()

    emprendimientos = Emprendimientos.query.all()
    atractivos = Atractivos.query.all()
    servicios = Servicios.query.all()
    regiones = Regiones.query.all()
    
    ubicaciones = []
    for item in itertools.chain(emprendimientos, atractivos):
        try:
            lat = item.ubicacion.split(",")[0]
            lng = item.ubicacion.split(",")[1]
            ubicaciones.append([item.nombre, lat, lng, item.__class__.__name__])
        except:
            pass

    # Se desordenan las listas para que no aparezcan siempre igual    
    random.shuffle(emprendimientos)
    random.shuffle(atractivos)
    random.shuffle(regiones)

    return render_template("index.html", texts=Textos.query.all(),  
                atractions=atractivos, regiones=regiones, footer=Footer.query.all(), 
                emprendimientos=emprendimientos, data=ubicaciones)

@app.route("/emprendimientos/", methods=['GET', 'POST'])
def emprendimientos():
    link_a_imagen()

    emprendimientos = Emprendimientos.query.all()
    random.shuffle(emprendimientos)
    atractivos = Atractivos.query.all()
    random.shuffle(atractivos)
    regiones = Regiones.query.all()
    random.shuffle(regiones)

    numeroAtractivos = len(atractivos)

# *********** para el filtro **************************
    emprendimientosYAtractivos = []
    for item in emprendimientos:
        try:
            listaDeCosas = []
            for atractivo in item.atractivos:
                listaDeCosas.append(atractivo.tipo.nombre)
            for servicio in item.servicios:
                listaDeCosas.append(servicio.nombre.replace(" ", "-"))
            listaDeCosas.append(item.region.nombre.replace(" ", "-"))
            emprendimientosYAtractivos.append([item.nombre, listaDeCosas])
        except:
            pass

    textoEmprendedor = Textos.query.all()[0].emprendedor

    return render_template("emprendimientos.html", lista=emprendimientos, texts=Textos.query.all(), atractivos=atractivos, 
    servicios=Servicios.query.all(), regiones=regiones, footer=Footer.query.all(), numeroAtractivos=numeroAtractivos,
    emprendimientosYAtractivos = json.dumps(emprendimientosYAtractivos), tipos=Tipos.query.all(), 
    textoEmprendedor = textoEmprendedor) 

@app.route("/atractivos/")
def atractivos():
    link_a_imagen()

    atractivos = Atractivos.query.all()
    random.shuffle(atractivos)
    regiones = Regiones.query.all()
    tipos = Tipos.query.all()

    # *********** para el filtro **************************
    atractivosYCosas = []
    for item in atractivos:
        try:
            listaDeCosas = []
            for tipo in item.tipos:
                listaDeCosas.append(tipo.nombre.replace(" ", "-"))
            for region in item.regiones:
                listaDeCosas.append(region.nombre.replace(" ", "-"))
            atractivosYCosas.append([item.nombre, listaDeCosas])
        except:
            pass

    return render_template("atractivos.html", lista = atractivos, 
                texts = Textos.query.all(), footer = Footer.query.all(),
                regiones = regiones, tipos = tipos, atractivosYCosas = atractivosYCosas)

@app.route("/regiones/")
def regiones():
    link_a_imagen()

    regiones = Regiones.query.all()
    random.shuffle(regiones)

    atractivos=Atractivos.query.all()
    random.shuffle(atractivos)

    return render_template("regiones.html", lista=regiones, 
                        texts=Textos.query.all(), footer=Footer.query.all(),
    atractivos=atractivos)

@app.route("/mapa/")
def mapa():
    emprendimientos = Emprendimientos.query.all()
    atractivos = Atractivos.query.all()
    servicios = Servicios.query.all()
    regiones = Regiones.query.all()
    
    ubicaciones = []
    for item in itertools.chain(emprendimientos, atractivos):
        try:
            lat = item.ubicacion.split(",")[0]
            lng = item.ubicacion.split(",")[1]
            ubicaciones.append([item.nombre, lat, lng, item.__class__.__name__])
        except:
            pass

    # *********** para el filtro **************************
    emprendimientosYAtractivos = []
    for item in emprendimientos:
        try:
            listaDeCosas = []
            for atractivo in item.atractivos:
                listaDeCosas.append(atractivo.tipo.nombre.replace(" ", "-"))
            for servicio in item.servicios:
                listaDeCosas.append(servicio.nombre.replace(" ", "-"))
            listaDeCosas.append(item.region.nombre.replace(" ", "-"))
            emprendimientosYAtractivos.append([item.nombre.replace(" ", "-"), listaDeCosas])
        except:
            pass

    atractivosYTipos = []
    for item in atractivos:
        try:
            listaDeCosas = []
            listaDeCosas.append(item.tipo.nombre.replace(" ", "-"))
            for region in item.regiones:
                listaDeCosas.append(region.nombre.replace(" ", "-"))
            atractivosYTipos.append([item.nombre.replace(" ", "-"), listaDeCosas])
        except:
            pass

    return render_template("mapa.html", texts=Textos.query.all(), data=ubicaciones, atractivos=atractivos, servicios=Servicios.query.all(), 
    regiones=regiones, footer=Footer.query.all(), emprendimientos=emprendimientos, tipos=Tipos.query.all(),
    emprendimientosYAtractivos=json.dumps(emprendimientosYAtractivos), atractivosYTipos=json.dumps(atractivosYTipos))

@app.route("/eventos/")
def eventos():
    link_a_imagen()

    eventos = Evento.query.all()

    eventosYCosas = []
    for item in eventos:
        try:
            listaDeCosas = []
            for tipo in item.tipo_evento:
                listaDeCosas.append(tipo.nombre.replace(" ", "-"))
            for region in item.regiones:
                listaDeCosas.append(region.nombre.replace(" ", "-"))
            eventosYCosas.append([item.nombre, listaDeCosas])
        except:
            pass

    print(eventosYCosas)

    # ordenando los eventos por fecha:
    eventos = Evento.query.order_by(Evento.inicio).all()

    return render_template("eventos.html", texts=Textos.query.all(), footer=Footer.query.all(), eventos=eventos,
    regiones=Regiones.query.all(), tipos=Tiposeventos.query.all(), eventosYCosas=eventosYCosas)

@app.route("/acerca-de/")
def acerca():
    
    titulo = "Sobre la página"

    textoHTML = Textos.query.all()[0].textoAcercaDe
    return render_template("info.html", texts=Textos.query.all(), footer=Footer.query.all(),
                            textoHTML = textoHTML, titulo = titulo)

@app.route("/creditos/")
def creditos():

    titulo = "Créditos de la página"

    textoHTML = Textos.query.all()[0].textoCreditos
    return render_template("info.html", texts=Textos.query.all(), footer=Footer.query.all(),
                            textoHTML = textoHTML, titulo = titulo)

@app.route("/<name>/")
def pagina(name):    
    link_a_imagen() 
    
    for item in Emprendimientos.query.all():
        if item.nombre == name:
            try:
                path = os.listdir(getPath() + '/static/img/emprendimientos/' 
                            + str(item.nombre) + "/otras/")
                imagenes = [getPath() + '/static/img/emprendimientos/' 
                            + str(item.nombre) + "/otras/" + file for file in path]
            except:
                imagenes = []
            for i in range(len(imagenes)):
                imagenes[i] = '/static/img/emprendimientos/' 
                + imagenes[i].split('/static/img/emprendimientos/')[1]
                
            return render_template("auxiliar.html", tipo="emprendimiento", 
                    item=item, texts=Textos.query.all(), footer=Footer.query.all(), imagenes=imagenes)
    
    for item in Atractivos.query.all():
        if item.nombre == name:
            try:
                path = os.listdir(getPath() + '/static/img/atractivos/' 
                            + str(item.nombre) + "/otras/")
                imagenes = [getPath() + '/static/img/atractivos/' 
                            + str(item.nombre) + "/otras/" + file for file in path]
            except:
                imagenes = []
            for i in range(len(imagenes)):
                imagenes[i] = '/static/img/atractivos/' 
                + imagenes[i].split('/static/img/atractivos/')[1]
                
            return render_template("auxiliar.html", tipo="atractivo", 
                    item=item, texts=Textos.query.all(), footer=Footer.query.all(), imagenes=imagenes)
    
    for item in Regiones.query.all():
        if item.nombre == name:
            try:
                path = os.listdir(getPath() + '/static/img/regiones/' 
                            + str(item.nombre) + "/otras/")
                imagenes = [getPath() + '/static/img/regiones/' 
                            + str(item.nombre) + "/otras/" + file for file in path]
            except:
                imagenes = []                        

            for emprendimiento in item.emprendimientos:
                try:
                    path = os.listdir(getPath() + '/static/img/emprendimientos/' 
                            + str(emprendimiento.nombre) + "/otras/")
                    imagenes2 = [getPath() + '/static/img/emprendimientos/' 
                            + str(emprendimiento.nombre) + "/otras/" + file for file in path]
                except:
                    imagenes2 = []
                for imagen in imagenes2:
                    imagenes.append(imagen)

            for atractivo in item.atractivos:
                try:
                    path = os.listdir(getPath() + '/static/img/atractivos/' 
                            + str(atractivo.nombre) + "/otras/")
                    imagenes3 = [getPath() + '/static/img/atractivos/' 
                            + str(atractivo.nombre) + "/otras/" + file for file in path]
                except:
                    imagenes3 = []
                for imagen in imagenes3:
                    imagenes.append(imagen)

            for i in range(len(imagenes)):
                try:
                    imagenes[i] = '/static/img/regiones/' 
                    + imagenes[i].split('/static/img/regiones/')[1]
                except:
                    try: 
                        imagenes[i] = '/static/img/emprendimientos/' 
                        + imagenes[i].split('/static/img/emprendimientos/')[1]
                    except:
                        imagenes[i] = '/static/img/atractivos/' 
                        + imagenes[i].split('/static/img/atractivos/')[1]

            return render_template("auxiliar.html", tipo="region", 
                    item=item, texts=Textos.query.all(), footer=Footer.query.all(), imagenes=imagenes)

    imagen_error = ""
    path = os.listdir(getPath() + '/static/img')
    imagenes = [getPath() + '/static/img/' + file for file in path]
    for imagen in imagenes:
        if len(imagen.split("/static/img/error")) > 1:
            imagen_error = "/static/img/error" +  imagen.split("/static/img/error")[1]
            
    return render_template("error.html", texts=Textos.query.all(), footer=Footer.query.all(), imagen=imagen_error)

if __name__ == "__main__":    
    serve(app, listen='*:8080')