# Flask Web App: Página Web Osa

## Requerimientos / Requirements

Para instalar los requerimientos se debe ejecutar el siguiente comando: \
To install the requirements run the following command:

`$ pip3 install -r requirements.txt`

## Claves secretas / Secret keys
Dentro de instance/ se debe agregar un archivo llamado config.py, con el siguiente contenido, con claves seguras: \
Within instance/ a config.py file must be added, with the following content: 

```
SECRET_KEY = '<key>'
SECURITY_PASSWORD_SALT = '<password>'
```

## Crear base de datos / Create database

Para poder ingresar al panel de administración se debe tener un usuario con el rol 'creador' o 'admin'. Para crear la base de datos y agregar un usuario se debe ingresar primero al ambiente de Python: \
A user with a 'creador' or 'admin' role is required to access the admin panel. In order to create the database and add a user it's necessary to start a Python environment:


```
$ python3
```

Luego ejecutar los siguientes comandos (se pueden copiar y pegar todas las líneas a la vez): \
Then execute the following commands (you can copy and paste them all at once):

```
from app import db;
db.create_all();
from app import User, Role;
admin_role = Role(name="admin", descripcion="Tiene acceso al panel de administrador, sin las tablas 'User' y 'Role'.");
creador_role = Role(name="creador", descripcion="Tiene acceso completo al panel de administrador, y además puede crear usuarios.");
user = User(email="user@example.com", password="passwd", roles=[creador_role]);
db.session.add(admin_role);
db.session.add(user);
db.session.commit();
```

## Correr el código / Run the code

```
$ python3 app.py
```

## Imágenes / Images

Desde el panel de administración:
From the admin panel:

- Crear el directorio static/img/pagina_principal. Dentro de este directorio, la imagen encabezado se debe llamar "encabezado.jpg", y la imagen de fondo se debe llamar "fondo.jpg". \
Create the static/img/pagina_principal directory. Within this directory, the header image must be called "encabezado.jpg", and the background image must be called "fondo.jpg".

- En static/img, las imágenes de los logos de PiOsa, Tropicalización de la tecnología y la UCR se deben llamar, respectivamente, "piosa.png", "tropicalizacion.png" y "ucr.png". \
Within static/img, the PiOsa, Tropicalización de la tecnología and UCR logos must be called "piosa.png", "tropicalizacion.png" y "ucr.png".

- La imagen que se muestra cuando una página no existe es "error.\*" (cualquier extensión). \
The image that is shown when a page doesn't exist is "error.*" (any extension).

